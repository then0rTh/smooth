from sys import argv
from PIL import Image
from glob import iglob
from itertools import product
from os.path import join, basename

FORMAT = '.png'

_, src_dir, tg_dir = argv

for src_pth in iglob(join(src_dir, '*.*')):
    name, *dimensions = basename(src_pth).split('.')
    assert len(dimensions) == 2
    x, y = map(int, dimensions)
    tg_pth = join(tg_dir, name + FORMAT)
    img = Image.new('RGB', (x, y))
    with open(src_pth, 'rb') as file:
        for xy in product(range(x), range(y)):
            bts = file.read(3)
            assert len(bts) == 3
            img.putpixel(xy, tuple(map(int, bts)))
    img.save(tg_pth)
    # left, top, right, bot = img.getbbox()
    # with open(tg_pth + f'.{right}.{bot}', 'wb') as file:
    #     for xy in product(range(right), range(bot)):
    #         file.write(bytes(img.getpixel(xy)))
