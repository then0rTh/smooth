from itertools import chain, product
from sys import argv
import numpy as np

_, fname, reach, sigma, nearness, lsize, lthic, edge_dist = argv
reach = int(reach)
sigma = float(sigma)
nearness = int(nearness)
lsize = int(lsize)
lthic = bool(int(lthic))
edge_dist = int(edge_dist)
print(f"Kernel: reach={reach} | sigma={sigma} | laplaz({lsize}, {lthic}) | edge_dist={edge_dist}")

# //  (parametr sigma, velikost), try reach 15
# // 3*sigma ~ polomer jadra

mu = 0.0
x, y = np.meshgrid(np.linspace(-1, 1, reach), np.linspace(-1, 1, reach))
d = np.sqrt(x*x + y*y)
matrix = np.exp(-((d-mu)**2 / (2.0 * sigma**2)))

assert len(matrix) == reach and len(matrix[0]) == reach




if not lsize:
    laplaz = None
else:
    laplaz = [
        [   int(lsize//2 in (x + y, abs(x - y)))
            for x in range(lsize)
        ] for y in range(lsize//2+1)
    ]
    for y in range(1, lsize//2+1):
        cur, mod = 0, 1
        for x in range(lsize):
            if laplaz[y][x] and not cur:
                cur, mod = 1, 1
            elif cur:
                cur += mod
                laplaz[y][x] = cur
                if cur == y + 1:
                    mod = -1
    laplaz += laplaz[-2::-1]
    if lthic:
        for ox, oy in product(range(-1, 2), range(-1, 2)):
            laplaz[lsize//2 + ox][lsize//2 + oy] = 0
        contra = -sum(map(sum, laplaz)) // 16
        for ox, oy in product(range(-1, 2), range(-1, 2)):
            laplaz[lsize//2 + ox][lsize//2 + oy] = contra
    laplaz[lsize//2][lsize//2] = 0
    laplaz[lsize//2][lsize//2] = -sum(map(sum, laplaz))
if laplaz: assert sum(map(sum, laplaz)) == 0



txt = '''
immutable int REACH = {0};
immutable double[REACH][REACH] SELECT = {1};
immutable int EDGE_DISTANCE = {5};

immutable int LSIZE = {3};
'''
txt += 'immutable int[LSIZE][LSIZE] LAPLAZ = {4};'if lsize \
        else 'static int[][] LAPLAZ;//{4}'
def textize(mat, f = '{:f}'.format):
    if mat is None: return 'null'
    res = '],\n['.join(', '.join(map(f, row)) for row in mat)
    return '[\n['+ res +']\n]'
matrixtxt = textize(matrix)
laplaztxt = textize(laplaz, str)

with open(fname, 'w') as file:
    file.write(txt.format(reach, matrixtxt, nearness, lsize, laplaztxt, edge_dist))
