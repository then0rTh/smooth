import std.algorithm.comparison: max, min;
import std.math: abs, approxEqual;
import std.file: read, write;
import std.string: split;
import std.conv: to;


immutable double TRANSPARENT_VAL = -100;

struct RGB_raw
{
    ubyte r, g, b;
}


struct RGB
{
    double r, g, b;

    RGB opBinary(string op)(const RGB other) const
    {
        static if (op == "+") {
            if (this.transparent)
                return RGB(other);
            else if (other.transparent)
                return RGB(this);
            else
                RGB( this.r + other.r,
                     this.g + other.g,
                     this.b + other.b );
        } else static assert(0, "Operator "~op~" not implemented");
    }

    void opOpAssign(string op)(const RGB other)
    {
        static if (op == "+") {
            if (this.transparent) {
                this = other;
                return;
            }
            if (other.transparent)
                return;
            this.r += other.r;
            this.g += other.g;
            this.b += other.b;
        }
        else static assert(0, "Operator "~op~" not implemented");
    }

    RGB opBinary(string op)(const double coef)
    out (res)
    {
        const string msg = this.to!string ~ op ~ coef.to!string ~ " = " ~ res.to!string;
        assert(min(res.r, res.g, res.b) > 0 || approxEqual(0, min(res.r, res.g, res.b)), msg);
        assert(max(res.r, res.g, res.b) < 256, msg);
    }
    body
    {
        static if (op == "*")
            return RGB(this.r * coef, this.g * coef, this.b * coef);
        else static if (op == "/")
            return RGB(this.r / coef, this.g / coef, this.b / coef);
        else static assert(0, "Operator "~op~" not implemented");
    }

    RGB_raw opCast(T : RGB_raw)() const
    in
    {
        // assert(this.r < 256 && this.g < 256 && this.b < 256);
        assert(this.r >= 0 && this.g >= 0 && this.b >= 0);
    }
    body
    {
        return RGB_raw( to!ubyte(min(255, this.r)),
                        to!ubyte(min(255, this.g)),
                        to!ubyte(min(255, this.b)) );
    }

    @property
    bool transparent() const
    {
        return this.r == TRANSPARENT_VAL;
    }

    @property
    RGB grayscale() const
    {
        // return (this.r + this.g + this.b) / 3;
        const double gray = 0.299 * this.r + 0.587 * this.g + 0.114 * this.b;
        return RGB(gray,gray,gray);
    }
}

immutable RGB BLACK = {0, 0, 0};
immutable RGB WHITE = {250, 250, 250};



void extractSize(const string fname, out size_t x, out size_t y) pure
{
    const string[2] dimensions = fname.split('.')[1..3];
    x = to!size_t(dimensions[0]);
    y = to!size_t(dimensions[1]);
}

RGB[] conv(const ubyte[] data) pure
{
    RGB[] arr;
    arr.length = data.length/3;
    for (size_t i = 0, off = 0; off < data.length; off+=3, ++i)
        arr[i] = RGB(data[off], data[off+1], data[off+2]);
    return arr;
}


class Image
{
    private RGB[] pixels;
    private string filename;

    public const size_t x;
    public const size_t y;

    this(const Image orig)
    {
        this.filename = orig.filename;
        this.pixels = orig.pixels.dup;
        this.x = orig.x;
        this.y = orig.y;
    }

    this(const string filename)
    {
        this.filename = filename;
        size_t a, b;
        extractSize(filename, a, b);
        this.x = a;
        this.y = b;
        this.pixels = conv(cast(ubyte[]) read(filename));
    }

    this(const size_t x, const size_t y)
    {
      this.filename = "";
      this.pixels = new RGB[x*y];
      this.x = x;
      this.y = y;
    }

    @property
    public Image dup() const
    {
        return new Image(this);
    }

    @property
    public Image grayscale() const
    {
        Image i = new Image(this.x, this.y);
        for (size_t x = 0; x < this.x; ++x)
            for (size_t y = 0; y < this.y; ++y)
                i[x, y] = this[x, y].grayscale;
        i.filename = this.filename;
        return i;
    }

    public RGB opIndex(const size_t a, const size_t b) const
    {
        return this.pixels[b * this.x + a];
    }

    public RGB opIndexAssign(const RGB value, const size_t a, const size_t b)
    {
        this.pixels[b * this.x + a] = value;
        return value;
    }

    Image opBinary(string op)(const Image other) const
    {
        static if (op == "+") {
            Image i = this.dup;
            return i += other;
        } else static assert(0, "Operator "~op~" not implemented");
    }

    void opOpAssign(string op)(const Image other)
    {
        static if (op == "+") {
            this.pixels += other.pixels;
        }
        else static assert(0, "Operator "~op~" not implemented");
    }

    public void overwrite() const
    {
        this.saveAs(this.filename);
    }

    public void saveAs(const string filename) const
    {
        const size_t len = this.pixels.length;
        RGB_raw[] raw = new RGB_raw[len];
        for (size_t i = 0; i < len; ++i)
            raw[i] = cast(RGB_raw) this.pixels[i];
        write(filename, raw);
    }

    /// For testing purposes only
    public void drawRect(const size_t size)
    {
        for (size_t x = 0; x < size; ++x)
            for (size_t y = 0; y < size; ++y)
                this[x, y] = BLACK;
    }
}
