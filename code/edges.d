import std.algorithm.comparison: max, min;
// import std.algorithm.searching: minElement;
import std.algorithm.iteration: map, sum;
import std.math: pow, sqrt, round, approxEqual;
import std.stdio: write, writeln;
import std.array: array;
import std.conv: to;

import util: distance, fits, mksum;
import img: Image, RGB, BLACK, WHITE;
import kernel: LSIZE, LAPLAZ, EDGE_DISTANCE;


static assert(LSIZE == 0 || LSIZE % 2 == 1, "LSIZE must be odd");



void mkEdges(Image img)
{
    static if (LSIZE == 0)
        return;
    else {
        const Image orig = img.grayscale;
        mkEdges(img, orig);
    }
}

void mkEdges(Image img, const Image orig)
{
    static if (LSIZE == 0)
        return;
    else {
        for (int x = 0; x < img.x; ++x)
            for (int y = 0; y < img.y; ++y)
                if (isEdge(orig, x, y))
                    img[x, y] = BLACK;
    }
}

bool isEdge(const Image img, const int X, const int Y)
{
    double total = 0;
    for (int offY = -LSIZE/2;  offY <= LSIZE/2;  ++offY)
        for (int offX = -LSIZE/2;  offX <= LSIZE/2;  ++offX)
            if (fits(img, X, Y, offX, offY))
                total += img[X + offX, Y + offY].r //NOTE: img is grayscaled
                         * LAPLAZ[offX + LSIZE/2][offY + LSIZE/2];
    return total > EDGE_DISTANCE;
}
