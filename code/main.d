import std.stdio: write, writeln;
import std.conv: to;

import img: Image;
import smooth: smooth;
import edges: mkEdges;


void main(string[] args) {
    string tg = args[1];
    int n = 1;
    if (args.length > 2)
        n = to!int(args[2]);
    int gray = 0;
    if (args.length > 3)
        gray = to!int(args[3]);

    Image i = new Image(tg);
    while (n--)
        i.smooth();
    i.mkEdges();

    if (gray)
        i = i.grayscale;

    i.overwrite();
}
