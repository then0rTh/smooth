import std.math: pow, sqrt, approxEqual;
import std.algorithm.iteration: map, sum;

import img: Image, RGB;



double mksum(const double[][] mat) pure
{
    return sum(mat.map!sum);
}


double distance(const RGB A, const RGB B) pure
{
    // const double tmp = pow(A.r - B.r, 2) + pow(A.g - B.g, 2) + pow(A.b - B.b, 2);
    return sqrt(pow(A.r - B.r, 2) + pow(A.g - B.g, 2) + pow(A.b - B.b, 2));
}


bool fits(const Image img, const size_t X, const size_t Y, const int offX = 0, const int offY = 0) pure
{
    return (X + offX >= 0)
        && (Y + offY >= 0)
        && (X + offX < img.x)
        && (Y + offY < img.y);
}
