import std.algorithm.comparison: max, min;
// import std.algorithm.searching: minElement;
// import std.algorithm.iteration: map, sum;
import std.math: approxEqual, cos, PI;
import std.stdio: write, writeln;
import std.array: array;
import std.conv: to;

import util: distance, fits, mksum;
import img: Image, RGB, BLACK, WHITE;
import kernel: REACH, SELECT;

static assert(REACH % 2 == 1, "REACH must be odd");



void smooth(Image img)
{
    const Image orig = img.dup;
    for (int x = 0; x < img.x; ++x)
        for (int y = 0; y < img.y; ++y)
            img[x, y] = compute(orig, x, y);
}


RGB compute(const Image img, const int X, const int Y)
{
    const double[][] select = mkSelect(img, X, Y);
    RGB res = RGB(0, 0, 0);
    for (int offY = -REACH/2;  offY <= REACH/2;  ++offY)
        for (int offX = -REACH/2;  offX <= REACH/2;  ++offX)
            if (select[offY+REACH/2][offX+REACH/2])
                res += img[X + offX, Y + offY]
                        * select[offY+REACH/2][offX+REACH/2];
    return res;
}


const(double[][]) mkSelect(const Image img, const int X, const int Y)
out (res)
{
    const string msg = res.to!string;
    assert(mksum(res) < 1.0 || approxEqual(mksum(res), 1.0), msg);
    double m = 1.0;
    for (int y = 0; y < res.length; ++y)
        for (int x = 0; x < res[0].length; ++x)
            m = min(m, res[y][x]);
    assert(m > 0 || approxEqual(m, 0), msg);
}
body
{
    double[][] select;
    select.length = REACH;
    for (int offY = 0; offY < REACH; ++offY)
    {
        select[offY].length = REACH;
        for (int offX = 0; offX < REACH; ++offX)
        {
            const int x = X - REACH/2 + offX, y = Y - REACH/2 + offY;
            select[offY][offX] = (fits(img, x, y))
                               ? SELECT[offY][offX]
                                    * nearCoef(img[x, y], img[X, Y])
                               : 0;
        }
    }
    normalize(select);
    return select;
}



void normalize(double[][] mat) pure
{
    const double total = mksum(mat);
    const size_t lenY = mat.length, lenX = mat[0].length;
    for (size_t y = 0; y < lenY; ++y)
        for (size_t x = 0; x < lenX; ++x)
            if (total) mat[y][x] /= total;
            else mat[y][x] = (x == lenX/2) && (y == lenY/2);
}


double nearCoef(const RGB A, const RGB B) pure
{
    const dist = distance(A, B);
    if (dist > 50) return 0;
    return cos(dist/PI/5)/2 + 0.5;
}
