'''
use do-it.sh args as params(-o is automatic), like:
localhost:5000/n=2&sigma=1&reach=7
'''
from http.server import HTTPServer, BaseHTTPRequestHandler
from subprocess import run, PIPE, CalledProcessError
from operator import methodcaller
from functools import wraps
from glob import iglob

base = '''
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>AIM-8</title>
        <style>
            body img {{ display: block; }}
            aside {{ position: absolute; right: 1em; }}
        </style>
    </head>
    <body>
        <aside>
            {}
        </aside>
        %s
    </body>
</html>
'''


opt = '<li>{}</li>'.format
img = '<img src="{0}" alt="{0}">'.format

class RequestHandler(BaseHTTPRequestHandler):
    @property
    def base(self) -> str:
        res = run(['grep', r'^\W*-.*)', 'do-it.sh'], stdout=PIPE, stderr=PIPE)
        if res.returncode:
            return base.format('')
        tmp = (x.strip('-)') for x in res.stdout.decode().split() if x != '-o)')
        aside = '<h3>Options</h3> <ul>'
        aside += '\n'.join(map(opt, tmp)) + '</ul>'
        return base.format(aside)

    def _get_params(self):
        if '?' not in self.path or '=' not in self.path:
            return ()
        ps = map(methodcaller('split', '='), self.path.split('?')[1].split('&'))
        return sum((('-'+a, b) for a, b in ps), ())

    def _response(self):
        print("Extracting url params:")
        params = self._get_params()
        # print(params)
        print("Done.")
        result = run(['sh', 'do-it.sh', '-o', *params], check=True, stderr=PIPE)
        print(result)
        return self.base % '\n'.join(map(img, iglob('result/*')))

    def img(self) -> bool:
        try:
            with open('.'+self.path, 'rb') as file:
                data = file.read()
        except IOError:
            return False
        else:
            self.head('image/png')
            self.wfile.write(data)
            return True

    def head(self, typ='text/html', code=200):
        self.send_response(code)
        self.send_header('Content-type', typ)
        self.end_headers()

    def do_GET(self):
        if self.img():
            return
        try:
            response = self._response()
        except CalledProcessError as e:
            print(RED, 'BAD ARGS:', e, NOCOLOR)
            self.send_error(400, e.stderr.decode().strip())
        except Exception as e:
            print(RED, 'ERROR:', type(e).__name__, e, NOCOLOR)
            self.send_error(500)
        else:
            self.head()
            self.wfile.write(response.encode())

##########################################
# colors #
RED = '\033[1;31m'
# RED = '\033[0;31m'
YELLOW = '\033[1;33m'
NOCOLOR = '\033[0m'


addr, port = '', 5000
httpd = HTTPServer((addr, port), RequestHandler)
print(f'Listening on {addr or "0.0.0.0"}:{port}...')
httpd.serve_forever()
