if [ "$1" ]
then
    opts="-frelease"
    echo "Compiling release"
else
    opts="-fdebug -Wall"
fi
gdc $opts -Icode code/*.d
