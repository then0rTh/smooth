# for list of args with descriptions, see below

NUM=1
SIGMA=1.0
REACH=11
NEAR=30
LSIZE=5
LTHIC=0
EDGE_DISTANCE=30
GRAY=0
intArg()
{
    TMPVAR="$1"
    TMPVAR=$(echo "$TMPVAR + 0" | bc)
    echo "$TMPVAR"
}
while [ "$1" ]; do
    case "$1" in
        ###########################
        ### Commandline options ###
        -n)
            # repeat the "smooth filter" N times: -n 4
            NUM=`intArg $2`
            shift; shift
            ;;
        -o)
            # turn on release optimalizations: -o
            RELEASE=1
            shift
            ;;
        -sigma)
            # set sigma for the "smooth filter" kernel: -sigma 1.0
            SIGMA=`intArg $2`
            shift; shift
            ;;
        -reach)
            # set total width of the "smooth filter" kernel: -reach 19
            REACH=`intArg $2`
            shift; shift
            ;;
        -lsize)
            # set total width of the "edges filter" kernel: -lsize 9
            LSIZE=`intArg $2`
            shift; shift
            ;;
        -lthic)
            # make the "edges filter" kernel thicker: -lthic 1
            LTHIC=`intArg $2`
            shift; shift
            ;;
        -edge_dist)
            # the bigger, the fewer edges gets highlighted: -edge_dist 100
            EDGE_DISTANCE=`intArg $2`
            shift; shift
            ;;
        -gray)
            # produce grayscaled image: -gray 1
            GRAY=`intArg $2`
            shift; shift
            ;;
        *)
            echo "Unknown arg: $1" >&2
            exit 1
            ;;
    esac
done



INFO='\033[1;36m'
NOCOLOR='\033[0m'

kernelArgs="$REACH $SIGMA $NEAR $LSIZE $LTHIC $EDGE_DISTANCE"
python3 code/mk-kernel.py code/kernel.d $kernelArgs || exit "$?"

if [ ! "$RELEASE" ]
then
    echo "Release mode not enabled: enable it by passing -o"
fi
sh compile.sh "$RELEASE"  || exit "$?"

if [ ! -d src ]
then
    echo "Not found: src/ directory with images to process" >&2
    exit 1
fi


if [ -e raw ]
then
    rm -r raw
fi
mkdir raw
python3 decode.py src raw  || exit "$?"

if [ -e result ]
then
    rm -r result
fi
mkdir result
for img in raw/*.*
do
    echo "${INFO}Doing $img ($NUM times):${NOCOLOR}"
    time -f "  took %E" -o _tmp ./a.out "$img" "$NUM" "$GRAY"  config.ini  || exit "$?"
    cat _tmp
    rm _tmp
done
python3 encode.py raw result  || exit "$?"

echo "OK"
