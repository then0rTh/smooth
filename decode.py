from sys import argv
from PIL import Image
from glob import iglob
from itertools import product
from os.path import join, basename

_, src_dir, tg_dir = argv

for src_pth in iglob(join(src_dir, '*.*')):
    tg_pth = join(tg_dir, basename(src_pth).split('.')[0])
    img = Image.open(src_pth)
    left, top, right, bot = img.getbbox()
    with open(tg_pth + f'.{right}.{bot}', 'wb') as file:
        for xy in product(range(right), range(bot)):
            file.write(bytes(img.getpixel(xy)))
