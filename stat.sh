totaltotal=0
for suff in sh d py
do
    total=0
    for f in *.$suff code/*.$suff
    do
        if [ -e "$f" ]
        then
            X=`wc -l "$f" | cut -d' ' -f1,1`
            total=$((total + X))
        fi
    done
    echo ".$suff: $total lines"
    totaltotal=$((totaltotal + total))
done
echo "Total: $totaltotal lines"
